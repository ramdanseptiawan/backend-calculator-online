package com.ramdan.calculatoronline

import org.springframework.web.bind.annotation.*

@RestController
class CalculatorController {

    @GetMapping("/add")
    fun add(@RequestParam(value="numbers") numbers: Array<Double>): Double {
      return numbers.sum()
    }

    @GetMapping("/substract")
    fun substract(@RequestParam(value="numbers") numbers: Array<Double>): Double {
      var result = numbers[0]
        for ((index,value ) in numbers.withIndex())
        {
            if(index!=0) {
                result -= value
            }
        }
        return result
    }

    @GetMapping("/multiply")
    fun multiply(@RequestParam(value="numbers") numbers: Array<Double>): Double {
        var result = numbers[0]
        for ((index,value ) in numbers.withIndex())
        {
            if(index!=0) {
                result *= value
            }
        }
        return result
    }

    @GetMapping("/divide")
    fun divide(@RequestParam(value="number1") number1: Double,@RequestParam(value="number2")number2: Double): Double {
        var result = number1.div(number2)
        return result
    }

    @GetMapping("/splitEQ")
    fun splitEQ(@RequestParam(value="number1") number1: Double,@RequestParam(value="number2")number2: Double): Array<Double> {
        var result : MutableList<Double> = mutableListOf()
        for (i in 0..number2.toInt()){
            result.add(number1/number2)
        }
        return result.toTypedArray()
    }

    @GetMapping("/splitNum")
    fun splitNum(@RequestParam(value="numbers") numbers: Array<Int>): Int {
        var result = numbers[0]
        for ((index,value ) in numbers.withIndex())
        {
            if(index!=0) {
                result -= value
            }
        }
        return result
    }

    @GetMapping("/hello")
    fun greeting(@RequestParam(value = "name", defaultValue = "World") name: String) =  "Hello, $name"
}