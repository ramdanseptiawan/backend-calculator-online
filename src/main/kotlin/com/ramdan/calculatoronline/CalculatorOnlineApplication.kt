package com.ramdan.calculatoronline

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CalculatorOnlineApplication

fun main(args: Array<String>) {
	runApplication<CalculatorOnlineApplication>(*args)
}
